﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace Calculator
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            button0.Clicked += addExpression;
            button1.Clicked += addExpression;
            button2.Clicked += addExpression;
            button3.Clicked += addExpression;
            button4.Clicked += addExpression;
            button5.Clicked += addExpression;
            button6.Clicked += addExpression;
            button7.Clicked += addExpression;
            button8.Clicked += addExpression;
            button9.Clicked += addExpression;
            buttonDot.Clicked += addExpression;
            buttonPlus.Clicked += addExpression;
            buttonMinus.Clicked += addExpression;
            buttonMult.Clicked += addExpression;
            buttonDiv.Clicked += addExpression;
            buttonEqu.Clicked += evaluateExpression;
            buttonDel.Clicked += deleteExpression;
        }
        void addExpression(object sender, EventArgs e)
        {
            Button button = sender as Button;
            equation.Text += button.Text;
        }
        async void evaluateExpression(object sender, EventArgs e)
        {
            string exp = equation.Text;
            exp = exp.Replace("÷", "/").Replace("×", "*").Replace("−", "-");
            var result = new object();
            try
            {
                 result = new DataTable().Compute(exp, null);
            } catch (SyntaxErrorException)
            {
                await DisplayAlert("Alert", "Invalid expression.", "OK");
                return;
            }
            string res = result.ToString().Replace(",", ".");
            if (res.Contains("Infini"))
            {
                await DisplayAlert("Alert", "Division by 0 is forbidden.", "OK");
                res = equation.Text;
            }
            equation.Text = res.ToString();
        }
        void deleteExpression(object sender, EventArgs e)
        {
            string exp = equation.Text;
            if (exp.Length > 1)
                exp = exp.Substring(0, exp.Length - 1);
            else
                exp = "";
            equation.Text = exp;
        }
    }
}

